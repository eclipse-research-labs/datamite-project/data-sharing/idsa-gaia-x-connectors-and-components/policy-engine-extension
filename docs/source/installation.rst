Installation and Requirements
===========================
The execution of the policy engine is integrated in the Eclipse Data Space connector as an extension.
This is the list or requirements and preconditions needed to execute the policy engine.

* EDC connector installed
* Keycloak installed
* EDC configured in Keycloak

For the last two policies we have included a claim in the Identity provider/Keycloak for the connector registered as a client:

The next figure correspond with a "consumer-connector" client created at Keycloak:

.. image:: https://gitlab.eclipse.org/eclipse-research-labs/datamite-project/data-sharing/idsa-gaia-x-connectors-and-components/policy-engine-extension/-/raw/main/doc/figures/consumer_connector_client.png?ref_type=heads

Once client is created you must go to "Client scopes" tab and click on "consumer-connector-dedicated" link

.. image:: https://gitlab.eclipse.org/eclipse-research-labs/datamite-project/data-sharing/idsa-gaia-x-connectors-and-components/policy-engine-extension/-/raw/main/doc/figures/consumer_connector_client_client_scope.png

Click on "Add Mapper" button and select "By configuration" option:

.. image:: https://gitlab.eclipse.org/eclipse-research-labs/datamite-project/data-sharing/idsa-gaia-x-connectors-and-components/policy-engine-extension/-/raw/main/doc/figures/create_claim_by_default.png

Create two "Hardcoded Claim" claims:

.. image:: https://gitlab.eclipse.org/eclipse-research-labs/datamite-project/data-sharing/idsa-gaia-x-connectors-and-components/policy-engine-extension/-/raw/main/doc/figures/create_claim_harcoded.png

And fill the information for purpose and location claims:

* Location claim
.. image:: https://gitlab.eclipse.org/eclipse-research-labs/datamite-project/data-sharing/idsa-gaia-x-connectors-and-components/policy-engine-extension/-/raw/main/doc/figures/keycloak_location_claim.PNG
* Purpose claim
.. image:: https://gitlab.eclipse.org/eclipse-research-labs/datamite-project/data-sharing/idsa-gaia-x-connectors-and-components/policy-engine-extension/-/raw/main/doc/figures/keycloak_purpose_claim.PNG

The result is:
.. image:: https://gitlab.eclipse.org/eclipse-research-labs/datamite-project/data-sharing/idsa-gaia-x-connectors-and-components/policy-engine-extension/-/raw/main/doc/figures/keycloak_claim1.PNG

EDC Asset model in the EDC linked with the ODRL policy (this is done in the contract definition)

4.1.- Create asset
4.2.- Create policy (N-times, location, purpose policy)
4.3.- Create contract definition --> Link asset to policy
4.4.- Negotiate contract: in this steps is where the enforcement of the policies starts working, if the policy defined in the contract definition it is the right one the data transfer will be successful.

