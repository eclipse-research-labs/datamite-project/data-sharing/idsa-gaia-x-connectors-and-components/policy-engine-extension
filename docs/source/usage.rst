Usage
=====

* build.gradle.kts

Include these line in build.gradle.kts file:

.. code:: kotlin

    implementation(libs.edc.contract.spi)
    implementation(libs.edc.policy.engine.spi)

2.- Add an entry at "org.eclipse.edc.spi.system.ServiceExtension" file

.. image:: https://gitlab.eclipse.org/eclipse-research-labs/datamite-project/data-sharing/idsa-gaia-x-connectors-and-components/policy-engine-extension/-/raw/main/doc/figures/service_extension.png
