Introduction
=======================

**Policy engine extension**

This project is intended to provide the data policy usage definition and enforcement within the EDC connector. The module is not accessible from the other modules directly. It contains the code of the EDC policy extension which implements the policy engine for the following policies:

**Time Interval Usage**: allows data usage for a specific time interval.

::

    {
      "@context":{
        "@vocab": "https://w3id.org/edc/v0.0.1/ns/"
      },
      "@id":"aPolicyTimeInterval",
      "@type":"edc:PolicyDefinition",
      "policy": {
         "@context": "http://www.w3.org/ns/odrl.jsonld",
         "@type": "Set",
         "permission": [
           {
             "action": "use",
             "constraint": [
               {
                 "@type": "AtomicConstraint",
                 "leftOperand": "https://w3id.org/edc/v0.0.1/ns/timeInterval",
                 "rightOperand":{
                    "@type":"xsd:date",
                    "@value":"2022-12-31T23:00:00.000Z"
                 },
                 "operator": {
                   "@id": "odrl:gteq"
                 }
               },
               {
                 "@type": "AtomicConstraint",
                 "leftOperand": "https://w3id.org/edc/v0.0.1/ns/timeInterval",
                 "rightOperand":{
                    "@type":"xsd:date",
                    "@value":"2024-09-30T23:00:00.000Z"
                 },
                 "operator": {
                   "@id": "odrl:lteq"
                 }
               }
             ]
           }
         ],
         "prohibition": [],
         "obligation": []
    }
    }

**Location Usage**: allows data usage for a specific location.

Example:

::

    {
      "@context":{
        "@vocab": "https://w3id.org/edc/v0.0.1/ns/"
      },
      "@id":"aPolicyRegion",
      "@type":"edc:PolicyDefinition",
      "policy": {
         "@context": "http://www.w3.org/ns/odrl.jsonld",
         "@type": "Set",
          "permission": [
               {
                   "action": "use",
                   "constraint": {
                       "@type": "AtomicConstraint",
                       "leftOperand": "https://w3id.org/edc/v0.0.1/ns/regionLocation",
                       "operator": "odrl:eq",
                       "rightOperand": "eu"
                   }
               }
           ]
      }
    }

**Purpose Usage**: allows data usage for specific purposes.

Example:

::

    {
       "@context":{
       "@vocab": "https://w3id.org/edc/v0.0.1/ns/"
      },
     "@id":policy_id,
     "@type":"edc:PolicyDefinition",
      "policy": {
        "@context": "http://www.w3.org/ns



