/*
 *  Copyright (c) 2024 Fraunhofer Institute for Software and Systems Engineering
 *
 *  This program and the accompanying materials are made available under the
 *  terms of the Apache License, Version 2.0 which is available at
 *  https://www.apache.org/licenses/LICENSE-2.0
 *
 *  SPDX-License-Identifier: Apache-2.0
 *
 *  Contributors:
 *       Fraunhofer Institute for Software and Systems Engineering - initial API and implementation
 *
 */

package com.tecnalia.connector.policy;

import org.eclipse.edc.participant.spi.ParticipantAgent;
import org.eclipse.edc.participant.spi.ParticipantAgentPolicyContext;
import org.eclipse.edc.policy.engine.spi.AtomicConstraintRuleFunction;
import org.eclipse.edc.policy.engine.spi.PolicyContext;
import org.eclipse.edc.policy.model.Operator;
import org.eclipse.edc.policy.model.Permission;
import org.eclipse.edc.spi.monitor.Monitor;

import static java.lang.String.format;

import java.util.Collection;
import java.util.Objects;
import java.util.Map;


public class PurposeConstraintFunction<C extends ParticipantAgentPolicyContext> implements AtomicConstraintRuleFunction<Permission, C> {
    private Monitor monitor;

    public PurposeConstraintFunction (Monitor monitor) {
        this.monitor = monitor;
    }

    public static <C extends ParticipantAgentPolicyContext> PurposeConstraintFunction<C> create(Monitor monitor) {
        return new PurposeConstraintFunction<>(monitor) {
        };
    }

    @Override
    public boolean evaluate(Operator operator, Object rightValue, Permission rule, C context) {
        var participantId = context.participantAgent().getIdentity();
        var urlPurpose = context.participantAgent().getClaims().get("url_purpose");

        monitor.debug(format("PURPOSE Policy consumer ID = %s", participantId));
        monitor.debug(format("PURPOSE Policy urlPurpose = %s", urlPurpose));

        for (String key : context.participantAgent().getClaims().keySet()) {
            monitor.debug(format("PURPOSE Policy claims %s = %s", key, context.participantAgent().getClaims().get(key)));
        }
        monitor.debug("PURPOSE Policy ATTRIBUTES list");
        for (String key : context.participantAgent().getAttributes().keySet()) {
            monitor.debug(format("PURPOSE Policy attributes %s = %s", key, context.participantAgent().getAttributes().get(key)));
        }
        monitor.debug(format("Operator = %s", operator));
        monitor.debug(format("Rightvalue = %s", rightValue));

        monitor.info(format("Evaluating constraint: purpose %s %s", operator, rightValue.toString()));
        return switch (operator) {
            case EQ -> Objects.equals(urlPurpose, rightValue);
            case NEQ -> !Objects.equals(urlPurpose, rightValue);
            case IN -> ((Collection<?>) rightValue).contains(urlPurpose);
            default -> false;
        };
    }
}


