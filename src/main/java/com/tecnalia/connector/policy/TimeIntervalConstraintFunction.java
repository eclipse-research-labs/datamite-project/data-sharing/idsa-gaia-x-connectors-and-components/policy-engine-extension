/*
 *  Copyright (c) 2024 Fraunhofer Institute for Software and Systems Engineering
 *
 *  This program and the accompanying materials are made available under the
 *  terms of the Apache License, Version 2.0 which is available at
 *  https://www.apache.org/licenses/LICENSE-2.0
 *
 *  SPDX-License-Identifier: Apache-2.0
 *
 *  Contributors:
 *       Fraunhofer Institute for Software and Systems Engineering - initial API and implementation
 *
 */

package com.tecnalia.connector.policy;


import org.eclipse.edc.participant.spi.ParticipantAgent;
import org.eclipse.edc.participant.spi.ParticipantAgentPolicyContext;
import org.eclipse.edc.policy.engine.spi.AtomicConstraintRuleFunction;
import org.eclipse.edc.policy.engine.spi.PolicyContext;
import org.eclipse.edc.policy.model.Operator;
import org.eclipse.edc.policy.model.Permission;
import org.eclipse.edc.spi.monitor.Monitor;

import java.time.ZonedDateTime;
import java.time.ZoneOffset;

import static java.lang.String.format;

public class TimeIntervalConstraintFunction<C extends ParticipantAgentPolicyContext> implements AtomicConstraintRuleFunction<Permission, C> {
    private final Monitor monitor;

    public TimeIntervalConstraintFunction(Monitor monitor) {
        this.monitor = monitor;
    }

    public static <C extends ParticipantAgentPolicyContext> TimeIntervalConstraintFunction<C> create(Monitor monitor) {
        return new TimeIntervalConstraintFunction<>(monitor) {
        };
    }

    @Override
    public boolean evaluate(Operator operator, Object rightValue, Permission rule, C context) {
        final var current = getCurrentDate();
        final var dateValue = getDateOf(rightValue.toString());
        monitor.info(format("Evaluating constraint: TimeInterval %s %s", operator, rightValue.toString()));
        return switch (operator) {
            case EQ -> current.isEqual(dateValue);
            case NEQ -> !current.isEqual(dateValue);
            case LT -> current.isBefore(dateValue);
            case LEQ -> !current.isAfter(dateValue);
            case GT -> current.isAfter(dateValue);
            case GEQ -> !current.isBefore(dateValue);
            default -> false;
        };
    }

    /**
     * Convert a string to a {@link ZonedDateTime}.
     *
     * @param calendar The time as string.
     * @return The new ZonedDateTime object.
     * @throws DateTimeParseException if the string could not be converted.
     */
    private static ZonedDateTime getDateOf(final String calendar) {
        return ZonedDateTime.parse(calendar);
    }

    /**
     * Get current system date.
     *
     * @return The date object.
     */
    private static ZonedDateTime getCurrentDate() {
        return ZonedDateTime.now(ZoneOffset.UTC);
    }

}

