import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar

plugins {
    `java-library`
    id("com.github.johnrengelman.shadow") version "8.1.1"
    id("java")
    `maven-publish`
}

group = "com.tecnalia.hpa.edc.extension"

version = "0.10.1-0.0.1-SNAPSHOT"
//version = "0.10.1"
val edc_version = "0.10.1"


repositories {
    mavenCentral()
}

dependencies {
    implementation("org.eclipse.edc:core-spi:$edc_version")
    implementation("org.eclipse.edc:policy-engine-spi:$edc_version")
    implementation("org.eclipse.edc:contract-spi:$edc_version")
    implementation("org.eclipse.edc:json-ld:$edc_version")
    implementation("org.eclipse.edc:catalog-spi:$edc_version")
}

tasks.withType<ShadowJar> {
    exclude("**/pom.properties", "**/pom.xml")
    mergeServiceFiles()
    // Lo seteamos pero no vale para nada, porque se va a subir con el de la publicación
    archiveFileName.set("${rootProject.name}-${project.version}.jar")
}
publishing {
    publications {
        create<MavenPublication>(rootProject.name) {
            artifactId = rootProject.name
            artifact(tasks.shadowJar.get().archiveFile.get())
        }
    }
    repositories {
        maven {
            name = "eclipseReleases"
            url = uri("https://repo.eclipse.org/content/repositories/research-datamite-snapshots/")
            //url = uri("https://repo.eclipse.org/content/repositories/research-datamite-releases/")
            credentials {
                username = System.getenv("REPO_USERNAME") ?: ""  // Usa la variable de entorno o un valor por defecto
                password = System.getenv("REPO_PASSWORD") ?: ""
            }
        }
        
    }
}
